#include "Renderer.hpp"

#include <stdexcept>
#include <glad/gl.h>
#include <GLFW/glfw3.h>


int main(int argc, char* argv[]) {

	glfwInit();

	auto window = glfwCreateWindow(800, 600, "Example", nullptr, nullptr);

	if (!window)
		throw std::runtime_error("Error creating glfw window");

	glfwMakeContextCurrent(window);

	if (!gladLoaderLoadGL())
		throw std::runtime_error("Error initializing glad");

	{

		auto positions{ std::to_array<float>({
			   -0.5f, -0.5f,
				0.5f, -0.5f,
				0.5f,  0.5f,
			   -0.5f,  0.5f
		 }) };

		auto indices{ std::to_array<unsigned int>({
				0,1,2,
				0,3,2
		}) };

		
		VertexArray vertex_array;

		VertexBuffer vertex_buffer{ positions.data(), sizeof(float) * positions.size() };

		VertexBufferLayout layout;
		layout.push<float>(2);

		vertex_array.addBuffer(vertex_buffer, layout);

		IndexBuffer index_buffer{ indices.data(), 6 };

		GLCall(unsigned int program = glCreateProgram();)

		Shader vertex_shader{ "shaders/vertex.glsl" };
		Shader fragment_shader{ "shaders/fragment.glsl" };

		Renderer renderer{};

		renderer.attachShader(vertex_shader.getShaderId());
		renderer.attachShader(fragment_shader.getShaderId());

		renderer.linkAndValidateProgram();
		renderer.useProgram(program);

		renderer.setUniform4f("u_Color", 1.0f, 0.5f, 0.3f, 1.0f);


		while (!glfwWindowShouldClose(window)) 
		{	
			renderer.draw(vertex_array, index_buffer);

			glfwSwapBuffers(window);

			glfwPollEvents();
		}

		GLCall(glDeleteProgram(program);)

		glfwDestroyWindow(window);

	}

	glfwTerminate();

	return 0;
}