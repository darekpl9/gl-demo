#include "Renderer.hpp"


int Renderer::getUniformLocation(const std::string& name)
{
	GLCall(int location = glGetUniformLocation(program_id, name.c_str()));

	if (location == -1)
	{
		std::cout << "Uniform: " << name << " doesn't exist!" << std::endl;
	}

	return location;
}

void Renderer::useProgram(unsigned int program_id)
{
	this->program_id = program_id;

	GLCall(glUseProgram(program_id);)
}

void Renderer::attachShader(unsigned int shader_id)
{	
	GLCall(glAttachShader(program_id, shader_id);)

	shader_ids.push_back(shader_id);
}

void Renderer::linkAndValidateProgram()
{
	GLCall(glLinkProgram(program_id);)

	int link_status;
	GLCall(glGetProgramiv(program_id, GL_LINK_STATUS, &link_status));

	if (link_status == GL_FALSE)
	{
		int length;
		GLCall(glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &length);)

			std::unique_ptr<char> message = std::make_unique<char>(length);
		GLCall(glGetProgramInfoLog(program_id, 1024, &length, message.get()));

		std::cout << "Failed to link program" << std::endl;
		std::cout << message << std::endl;

		GLCall(glDeleteProgram(program_id));
	}

	GLCall(glValidateProgram(program_id);)

	std::ranges::for_each(shader_ids, [](unsigned int shader_id) { glDeleteShader(shader_id); });
}

void Renderer::draw(const VertexArray& vertex_array, const IndexBuffer& index_buffer) const
{
	vertex_array.bind();
	index_buffer.bind();

	GLCall(glClear(GL_COLOR_BUFFER_BIT);)

	GLCall(glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);)
}

void Renderer::setUniform4f(const std::string& name, float v0, float v1, float v2, float v3)
{
	GLCall(glUniform4f(getUniformLocation(name), v0, v1, v2, v3);)
}
