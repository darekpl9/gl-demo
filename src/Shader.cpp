#include "Shader.hpp"


Shader::Shader(const std::string& filepath)
	: filepath(filepath)
{
	parse();

	GLCall(id = glCreateShader(type);)

	compile();
}

Shader::~Shader()
{
	GLCall(glDeleteShader(id);)
}

void Shader::parse()
{
	std::ifstream stream(filepath);

	std::string line;
	while (std::getline(stream, line))
	{
		if (line.find("//shader") != std::string::npos)
		{
			if (line.find("vertex") != std::string::npos)
			{
				type = GL_VERTEX_SHADER;
			}
			else if (line.find("fragment") != std::string::npos)
			{
				type = GL_FRAGMENT_SHADER;
			}
		}
		else
		{
			source += line + "\n";
		}
	}
}

void Shader::compile()
{
	const char* src_ptr = source.c_str();

	GLCall(glShaderSource(id, 1, &src_ptr, nullptr);)
	GLCall(glCompileShader(id);)

	int result;
	GLCall(glGetShaderiv(id, GL_COMPILE_STATUS, &result);)

	if (result == GL_FALSE)
	{
		int length;
		GLCall(glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length);)

		std::unique_ptr<char> message = std::make_unique<char>(length);
		GLCall(glGetShaderInfoLog(id, length, &length, message.get());)

		std::cout << "Failed to compile a shader!" << std::endl;
		std::cout << message << std::endl;

		GLCall(glDeleteShader(id);)
	}
}
