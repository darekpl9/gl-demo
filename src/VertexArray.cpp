#include "VertexArray.hpp"


VertexArray::VertexArray()
	: attrib_id(0)
{
	GLCall(glGenVertexArrays(1, &id);)
}

VertexArray::~VertexArray()
{
	GLCall(glDeleteVertexArrays(1, &id);)
}

void VertexArray::addBuffer(const VertexBuffer& vertex_buffer, const VertexBufferLayout& layout)
{
	bind();

	vertex_buffer.bind();

	const auto& elements = layout.getElements();

	unsigned int offset = 0;

	for (unsigned int i = 0; i < elements.size(); ++i)
	{
		const auto& element = elements[i];

		GLCall(glEnableVertexAttribArray(attrib_id + i);)
		GLCall(glVertexAttribPointer(attrib_id + i, element.count, element.type, element.normalized,
			layout.getStride(), reinterpret_cast<const void*>(offset));)

		offset += element.count * VertexBufferElement::getSizeOfType(element.type);
	}

	attrib_id += elements.size();
}

void VertexArray::bind() const
{
	GLCall(glBindVertexArray(id);)
}

void VertexArray::unbind() const
{
	GLCall(glBindVertexArray(0);)
}
