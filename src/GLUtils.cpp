#include "GLUtils.hpp"


void clearGLError()
{
	while (glGetError() != GL_NO_ERROR);
}


void printGLError(const char* function, const char* file, int line)
{
	while (GLenum error = glGetError())
	{
		std::cout << "[OpenGL Error] (" << error << "): " << function <<
			" " << file << ": " << line << std::endl;
	}
}