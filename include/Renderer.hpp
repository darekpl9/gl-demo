#pragma once

#include "Shader.hpp"
#include "IndexBuffer.hpp"
#include "VertexArray.hpp"
#include "GLUtils.hpp"

#include <memory>
#include <vector>
#include <algorithm>
#include <glad/gl.h>


class Renderer
{
private:
	int getUniformLocation(const std::string& name);

	unsigned int program_id;
	std::vector<unsigned int> shader_ids{};

public:
	Renderer() = default;
	
	void useProgram(unsigned int program_id);
	void attachShader(unsigned int shader_id);
	void linkAndValidateProgram();
	void draw(const VertexArray& vertex_array, const IndexBuffer& index_buffer) const;
	void setUniform4f(const std::string& name, float v0, float v1, float v2, float v3);
};