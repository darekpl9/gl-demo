#pragma once

#include <vector>
#include <glad/gl.h>
#include <iostream>


struct VertexBufferElement
{
	unsigned int type;
	unsigned int count;
	unsigned char normalized;

	static unsigned int getSizeOfType(unsigned int type)
	{
		switch (type)
		{
			case GL_FLOAT: return 4;
			case GL_UNSIGNED_INT: return 4;
			case GL_UNSIGNED_BYTE: return 1;
		}

		return 0;
	}
};


class VertexBufferLayout
{
private:
	std::vector<VertexBufferElement> elements;
	unsigned int stride;

public:
	VertexBufferLayout()
		: stride(0){} 

	template<typename T>
	void push(unsigned int count)
	{
		
	}

	template<>
	void push<float>(unsigned int count)
	{
		elements.push_back(VertexBufferElement(GL_FLOAT, count, GL_FALSE));
		stride += count * VertexBufferElement::getSizeOfType(GL_FLOAT);
	}

	template<>
	void push<unsigned int>(unsigned int count)
	{
		elements.push_back(VertexBufferElement(GL_UNSIGNED_INT, count, GL_FALSE));
		stride += count * VertexBufferElement::getSizeOfType(GL_UNSIGNED_INT);
	}

	template<>
	void push<unsigned char>(unsigned int count)
	{
		elements.push_back(VertexBufferElement(GL_UNSIGNED_BYTE, count, GL_TRUE));
		stride += count * VertexBufferElement::getSizeOfType(GL_UNSIGNED_BYTE);
	}

	inline const std::vector<VertexBufferElement> getElements() const { return elements; }

	inline unsigned int getStride() const { return stride; }
};