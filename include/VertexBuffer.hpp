#pragma once

#include <iostream>
#include <glad/gl.h>


class VertexBuffer
{
private:
	unsigned int id;

public:
	VertexBuffer(const void* data, unsigned int size);
	~VertexBuffer();

	void bind() const;
	void unbind() const;
};