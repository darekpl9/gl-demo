#pragma once

#include <iostream>
#include <glad/gl.h>


#define GLCall(x) clearGLError();\
	x;\
	printGLError(#x, __FILE__, __LINE__);


void clearGLError();
void printGLError(const char* function, const char* file, int line);