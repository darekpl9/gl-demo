#pragma once

#include "GLUtils.hpp"

#include <array>

#include <glad/gl.h>


class IndexBuffer
{
private:
	unsigned int id;
	unsigned int count;

public:
	IndexBuffer(const unsigned int* data, unsigned int count);
	~IndexBuffer();

	void bind() const;
	void unbind() const;
};