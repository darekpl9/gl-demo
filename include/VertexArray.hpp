#pragma once

#include "VertexBuffer.hpp"
#include "VertexBufferLayout.hpp"
#include "GLUtils.hpp"


class VertexArray
{
private:
	unsigned int id;
	unsigned int attrib_id;

public:
	VertexArray();
	~VertexArray();

	void addBuffer(const VertexBuffer& vertex_buffer, const VertexBufferLayout& layout);
	void bind() const;
	void unbind() const;
};