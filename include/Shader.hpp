#pragma once

#include "GLUtils.hpp"

#include <string>
#include <fstream>
#include <iostream>

#include <glad/gl.h>


class Shader
{
private:
	unsigned int id;
	std::string filepath;
	GLenum type;
	std::string source;

	void parse();
	void compile();

public:
	Shader(const std::string& filepath);
	~Shader();

	inline unsigned int getShaderId() const { return id; }
};